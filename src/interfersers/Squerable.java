package interfersers;

public interface Squerable {
    double getSquare();
}
