package fig;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rectangle extends Figure {

    private double a;
    private double b;

    private Rectangle(Color color, int lineVidth, Point s) {
        super(FIGURE_TYPE_RECTANGLE, color, lineVidth, s);
    }

    public Rectangle(Color color, int lineVidth, Point s, double a, double b) {
        this( color, lineVidth, s);
        this.a = a;
        this.b = b;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineVidth);
        gc.setStroke(color);
        gc.strokeRect(S.x-a/2,S.y-b/2,a,b);

    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public double getSquare() {
        return a*b;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("a=").append(a);
        sb.append(", b=").append(b);
        sb.append(", color=").append(color);
        sb.append(", lineVidth=").append(lineVidth);
        sb.append(", S=").append(S);
        sb.append('}');
        return sb.toString();
    }
}
