package fig;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static java.lang.Math.PI;

public class Circle extends Figure {
    private int radius;

    private Circle(Color color, int lineVidth, Point S) {
        super(FIGURE_TYPE_CIRCLE, color, lineVidth, S);
    }

    public Circle(Color color, int lineVidth, Point S, int radius) {
        this( color, lineVidth, S);
        this.radius = radius;

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineVidth);
        gc.setStroke(color);
        gc.strokeOval(S.x-radius,S.y-radius,radius*2,radius*2);

    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public double getSquare() {
        return radius*radius*PI;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append(", color=").append(color);
        sb.append(", lineVidth=").append(lineVidth);
        sb.append(", S=").append(S);
        sb.append('}');
        return sb.toString();
    }
}
