package fig;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.lang.Math;

import static java.lang.Math.sqrt;

public class Triangle extends Figure {

    private double base;

    private Triangle(Color color, int lineVidth, Point S) {
        super(FIGURE_TYPE_TRIANGLE, color, lineVidth, S);
    }

    public Triangle(Color color, int lineVidth, Point S, double base) {
        this( color, lineVidth, S);
        this.base = base;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineVidth);
        gc.setStroke(color);
        gc.strokeLine(S.x-base,S.y+base/2,S.x+base,S.y+base/2);
        gc.strokeLine(S.x-base,S.y+base/2,S.x,S.y-base);
        gc.strokeLine(S.x+base,S.y+base/2,S.x,S.y-base);

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public double getSquare() {
        return base*base;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("base=").append(base);
        sb.append(", color=").append(color);
        sb.append(", lineVidth=").append(lineVidth);
        sb.append(", S=").append(S);
        sb.append('}');
        return sb.toString();
    }
}
