package fig;

import interfersers.Printable;
import interfersers.Squerable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class Figure implements Printable, Squerable{
    public static final int FIGURE_TYPE_CIRCLE = 2;
    public static final int FIGURE_TYPE_TRIANGLE =1;
    public static final int FIGURE_TYPE_RECTANGLE = 0;

    private int type;

    protected Color color;
    protected int lineVidth;
    protected Point S;

    public abstract void draw(GraphicsContext gc);

    public Figure(int type, Color color, int lineVidth, Point S) {
        this.type = type;
        this.lineVidth = lineVidth;
        this.color = color;
        this.S = S;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getLineVidth() {
        return lineVidth;
    }

    public void setLineVidth(int lineVidth) {
        this.lineVidth = lineVidth;
    }

    public Point getS() {
        return S;
    }

    public void setS(Point S) {
        this.S = S;
    }
}
