package fig;

public class Point {

    double x,y;

    public Point(double x, double y){
        this.x=x;
        this.y=y;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Point{");
        sb.append("x=").append(x);
        sb.append(", y=").append(y);
        sb.append('}');
        return sb.toString();
    }
}
