package controler;

import fig.*;
import interfersers.Printable;
import interfersers.Squerable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;



public class MainScreenController implements Initializable {
    @FXML
    private Canvas canvas;
    private Figure[] figures;
    private Random rund;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        rund = new Random(System.currentTimeMillis());
    }

    private  Figure createFigure(Point point){
       Figure figure= null;
       switch (rund.nextInt(3)){
           case Figure.FIGURE_TYPE_RECTANGLE:
               int r1=rund.nextInt(101);
               int r2=rund.nextInt(101);
               figure = new Rectangle(Color.CORNFLOWERBLUE,rund.nextInt(5),point,r1<20?20:r1,r2<20?20:r2);

               break;
           case Figure.FIGURE_TYPE_CIRCLE:
               r1=rund.nextInt(51);
               figure = new Circle(Color.YELLOW,rund.nextInt(5),point,r1<20?20:r1);

           break;
           case Figure.FIGURE_TYPE_TRIANGLE:
               r1=rund.nextInt(51);
               figure = new Triangle(Color.RED,rund.nextInt(5),point,r1<20?20:r1);

               break;
           default:
               System.out.println("Unknown Figure type!");
       }
        return  figure;

    }

    private void addFigure(Figure figure){
        if(figure == null)
            return;
        if(figures[figures.length-1]==null) {
            figures[figures.length - 1] = figure;
        }else {
            Figure[] tmp = new Figure[figures.length+1];
            int index = 0;
            for(;index<figures.length;index++){
                tmp[index]= figures[index];
            }
            tmp[index]= figure;
            figures = tmp;
        }
    }

    private  void repaint(){
        canvas.getGraphicsContext2D().clearRect(0,0,1024,600);
        for (Figure figure : figures) {
            if(figure!=null){
                figure.draw(canvas.getGraphicsContext2D());
            }
        }

    }

    @FXML
    private void convasClicked(MouseEvent evt) {
        System.out.println(evt.getX());
        System.out.println(evt.getY());
        addFigure(createFigure(new Point(evt.getX(),evt.getY())));
        repaint();
        printFigure(figures);
        printOverAllSqar(figures);
    }
    private void printOverAllSqar(Squerable[] a){
        double b=0.0;
        for(Squerable sqa: a){
            if(sqa!=null){
                b+=sqa.getSquare();
            }
        }
        System.out.println("S="+b);
    }

    void printFigure(Printable[] p){
        for (Printable ar: p) {
            if(ar!=null) {
                ar.print();
            }
        }
    }
}